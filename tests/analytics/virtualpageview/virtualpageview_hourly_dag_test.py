from copy import deepcopy

import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "virtualpageview", "virtualpageview_hourly_dag.py"]


@pytest.fixture(name="dag")
def fixture_dag(dagbag):
    assert dagbag.import_errors == {}
    return dagbag.get_dag(dag_id="virtualpageview_hourly")


def test_virtualpageview_hourly_load(dag):
    assert dag is not None
    assert len(dag.tasks) == 3
    # Tests that the defaults from default_args are here.
    assert dag.default_args["do_xcom_push"] is False


def test_virtualpageview_hourly_success(dag, render_task):
    success_task = deepcopy(dag.get_task("write_success_file"))
    rendered_success_task = render_task(success_task)
    assert (
        rendered_success_task._url
        == "hdfs://analytics-hadoop/wmf/data/wmf/virtualpageview/hourly/year=2023/month=3/day=27/hour=1/_SUCCESS"
    )
