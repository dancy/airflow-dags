import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "mediawiki", "page_content_change_to_wikitext_raw.py"]


def test_mediawiki_build_dumps_from_events_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id="page_content_change_to_wikitext_raw")
    assert dag is not None
    assert len(dag.tasks) == 2
