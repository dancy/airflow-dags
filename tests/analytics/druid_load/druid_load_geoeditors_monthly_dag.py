import pytest


# This fixture defines the dag_path for the shared dagbag one
@pytest.fixture(name="dag_path")
def fixture_dagpath():
    return ["analytics", "dags", "druid_load", "druid_geoeditors_monthly_dag.py"]


def test_druid_load_geoeditors_monthly_dag_loaded(dagbag):
    assert dagbag.import_errors == {}
    monthly_dag = dagbag.get_dag(dag_id="druid_load_geoeditors_monthly")
    assert monthly_dag is not None
    assert len(monthly_dag.tasks) == 5
