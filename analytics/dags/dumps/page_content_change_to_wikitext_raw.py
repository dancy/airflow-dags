"""
************
NOTICE: This job is work in progress. If it fails, do please ignore.
************

This job transforms event data from event.mediawiki_page_content_change into an
Iceberg table meant to be the base to create dumps from.

We accomplish this by running a pyspark job that runs a MERGE INTO
that de-duplicates events and mutates the target table.

More info about the pyspark job at:
https://gitlab.wikimedia.org/repos/data-engineering/dumps/mediawiki-content-dump
"""

from datetime import datetime, timedelta

from analytics.config.dag_config import artifact, create_easy_dag
from wmf_airflow_common.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSubmitOperator
from wmf_airflow_common.sensors.hive import RangeHivePartitionSensor

props = DagProperties(
    # DAG settings
    start_date=datetime(2023, 3, 9),
    sla=timedelta(hours=2),
    conda_env=artifact("mediawiki-content-dump-0.1.0.dev0-pyspark.conda.tgz"),
    target_table="wmf_dumps.wikitext_raw_rc0",
    source_table="event.rc1_mediawiki_page_content_change",
    # Spark job tuning
    driver_memory="16G",
    executor_memory="8G",
    executor_cores="2",
    max_executors="64",
)


with create_easy_dag(
    dag_id="page_content_change_to_wikitext_raw",
    doc_md=__doc__,
    start_date=props.start_date,
    schedule="@hourly",
    tags=["hourly", "from_hive", "to_hive", "uses_spark", "requires_event_mediawiki_page_content_change"],
    sla=props.sla,
    max_active_runs=1,  # MERGEs will step into each other, let's run them serially.
    email="xcollazo@wikimedia.org"  # overriding alert email for now.
    # it's ok if this fails, and we don't want to alert ops week folk.
) as dag:
    sensor = RangeHivePartitionSensor(
        task_id="wait_for_mediawiki_page_content_change",
        table_name="event.rc1_mediawiki_page_content_change",
        from_timestamp="{{data_interval_start}}",
        to_timestamp="{{data_interval_end}}",
        granularity="@hourly",
        pre_partitions=[["datacenter=eqiad", "datacenter=codfw"]],
    )

    # Usage: merge_into.py source_table, target_table, year, month, day, hour
    args = [
        props.source_table,
        props.target_table,
        "{{data_interval_start.year}}",
        "{{data_interval_start.month}}",
        "{{data_interval_start.day}}",
        "{{data_interval_start.hour}}",
    ]
    merge_into = SparkSubmitOperator.for_virtualenv(
        task_id="spark_merge_into",
        virtualenv_archive=props.conda_env,
        # TODO: use an entry point def on python to make this prettier
        entry_point="lib/python3.11/site-packages/mediawiki_content_dump/merge_into.py",
        driver_memory=props.driver_memory,
        executor_memory=props.executor_memory,
        executor_cores=props.executor_cores,
        conf={
            "spark.dynamicAllocation.maxExecutors": props.max_executors,
            "spark.driver.extraJavaOptions": "-Xss4m",  # sql parsing of ~1k ORs creates deep recursion; bump stack size
        },
        launcher="skein",
        application_args=args,
    )

    sensor >> merge_into
