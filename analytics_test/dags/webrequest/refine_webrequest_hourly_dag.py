from datetime import datetime

from analytics.dags.webrequest.refine_webrequest_hourly_dag_factory import generate_dag

generate_dag(  # This generates an Airflow DAG https://github.com/apache/airflow/blob/2.5.1/airflow/utils/file.py#L358
    dag_id="refine_webrequest_hourly_test_text",
    webrequest_source="test_text",
    default_start_date=datetime(2023, 4, 1, 16),
    tags=["hourly", "uses_hql", "uses_email", "from_hive", "to_hive", "requires_wmf_raw_webrequest", "test"],
    import_dag_config_for_test_cluster=True,
    # Note: in the test cluster, we are manipulating a small subset of all web requests. Those the % of lost webrequests
    # is almost 100% by construction. Thus, checking for lost requests is irrelevant. We set the thresholds to 100% to
    # never trigger alarms.
    data_warning_threshold=100.0,
    data_error_threshold=100.0,
)
