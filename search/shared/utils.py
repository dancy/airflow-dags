from typing import Union


def parse_memory_to_mb(memory: Union[int, str]) -> int:
    """Parse spark style memory specification.

    Primarily a helper for reading and adjusting spark configuration.
    """
    if isinstance(memory, int):
        return memory
    if not isinstance(memory, str):
        raise Exception('Expected integer or string memory value')
    if memory.isdigit():
        return int(memory)

    suffixes = {
        'T': 2 ** 20,
        'G': 2 ** 10,
        'M': 1,
    }
    for suffix, scale in suffixes.items():
        if memory[-1].upper() == suffix:
            return int(memory[:-1]) * scale
    raise Exception('Unrecognized memory spec: {}'.format(memory))
